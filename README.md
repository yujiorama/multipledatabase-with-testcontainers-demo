# multiple database with testconatiners demo

## Requisites

* Java 11 ~
* Docker (Remote or Local)

## Run

```bash
./gradlew test
```

```powershell
.\gradlew.bat test
```

## License

MIT
