package multipledatabase.with.testcontainers.demo.app;

import multipledatabase.with.testcontainers.demo.app.component.CarDao;
import multipledatabase.with.testcontainers.demo.app.component.HouseDao;
import multipledatabase.with.testcontainers.demo.app.component.PeopleDao;
import multipledatabase.with.testcontainers.demo.app.config.AppConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.StandardEnvironment;

public class App {

    public static void main(final String[] argv) {
        final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        context.setEnvironment(new StandardEnvironment());

        final PeopleDao peopleDao = context.getBean(PeopleDao.class);
        peopleDao.findAll().forEach(System.out::println);
        final CarDao carDao = context.getBean(CarDao.class);
        carDao.findAll().forEach(System.out::println);
        final HouseDao houseDao = context.getBean(HouseDao.class);
        houseDao.findAll().forEach(System.out::println);

        context.destroy();
    }
}
