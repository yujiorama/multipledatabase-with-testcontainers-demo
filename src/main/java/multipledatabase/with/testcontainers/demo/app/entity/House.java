package multipledatabase.with.testcontainers.demo.app.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;

@Entity
@Table(name = "HOUSE")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Data
@ToString(includeFieldNames = true)
public class House implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private BigInteger id;

    @Column(name = "NAME", nullable = false, length = 20)
    private String name;

    @Column(name = "ADDRESS", nullable = false, length = 120)
    private String address;
}
