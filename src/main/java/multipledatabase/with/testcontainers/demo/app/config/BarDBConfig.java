package multipledatabase.with.testcontainers.demo.app.config;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Map;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
public class BarDBConfig {

    @Bean("barDataSource")
    public DataSource dataSource(final Environment environment) {

        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        final String url = String.format("jdbc:mysql://%s:%s/bar?useSSL=false", //
                environment.getProperty("bar.jdbc.host", "db01"), //
                environment.getProperty("bar.jdbc.port", "3306"));
        dataSource.setUrl(url);
        dataSource.setUsername(environment.getProperty("bar.jdbc.user", "test"));
        dataSource.setPassword(environment.getProperty("bar.jdbc.password", "test"));

        return dataSource;
    }

    @Bean("barEntityManagerFactory")
    public EntityManagerFactory entityManagerFactory(@Qualifier("barDataSource") final DataSource dataSource) {

        final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setPackagesToScan("multipledatabase.with.testcontainers.demo.app.entity");
        factoryBean.setPersistenceUnitName("bar");
        factoryBean.setPersistenceProvider(new HibernatePersistenceProvider());
        factoryBean.setJpaVendorAdapter(this.jpaVendorAdapter());
        factoryBean.setJpaProperties(this.jpaProperties());
        factoryBean.afterPropertiesSet();

        return factoryBean.getObject();
    }

    @Bean("barTransactionManager")
    public PlatformTransactionManager transactionManager(@Qualifier("barEntityManagerFactory") final EntityManagerFactory entityManagerFactory) {

        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        transactionManager.afterPropertiesSet();

        return transactionManager;

    }

    @Bean("barJpaVendorAdapter")
    public JpaVendorAdapter jpaVendorAdapter() {

        final HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
        jpaVendorAdapter.setDatabasePlatform("org.hibernate.dialect.MySQLDialect");
        jpaVendorAdapter.setGenerateDdl(false);
        jpaVendorAdapter.setShowSql(false);

        return jpaVendorAdapter;
    }

    @Bean("barJpaProperties")
    public Properties jpaProperties() {

        final Properties properties = new Properties();
        properties.putAll(Map.of(//
                "hibernate.hbm2ddl.auto", "none", //
                "hibernate.jdbc.batch_size", "30"//
        ));

        return properties;
    }

}
