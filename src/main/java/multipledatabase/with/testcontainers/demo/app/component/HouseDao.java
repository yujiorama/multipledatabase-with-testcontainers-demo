package multipledatabase.with.testcontainers.demo.app.component;

import multipledatabase.with.testcontainers.demo.app.entity.House;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
public class HouseDao {

    @PersistenceContext(unitName = "bar")
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return this.entityManager;
    }

    public void setEntityManager(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public House findById(final java.math.BigInteger id) {

        return this.getEntityManager().find(House.class, id);
    }

    public List<House> findAll() {

        return this.getEntityManager().createQuery(//
                String.format("from %s", House.class.getCanonicalName()), House.class)//
                .getResultList();
    }
}
