package multipledatabase.with.testcontainers.demo.app.component;

import multipledatabase.with.testcontainers.demo.app.entity.Car;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
public class CarDao {

    @PersistenceContext(unitName = "buz")
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return this.entityManager;
    }

    public void setEntityManager(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Car findById(final java.math.BigInteger id) {

        return this.getEntityManager().find(Car.class, id);
    }

    public List<Car> findAll() {

        return this.getEntityManager().createQuery(//
                String.format("from %s", Car.class.getCanonicalName()), Car.class)//
                .getResultList();
    }
}
