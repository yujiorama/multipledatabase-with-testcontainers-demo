package multipledatabase.with.testcontainers.demo.app.component;

import multipledatabase.with.testcontainers.demo.app.entity.People;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
public class PeopleDao {

    @PersistenceContext(unitName = "foo")
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return this.entityManager;
    }

    public void setEntityManager(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public People findById(final java.math.BigInteger id) {

        return this.getEntityManager().find(People.class, id);
    }

    public List<People> findAll() {

        return this.getEntityManager().createQuery(//
                String.format("from %s", People.class.getCanonicalName()), People.class)//
                .getResultList();
    }
}
