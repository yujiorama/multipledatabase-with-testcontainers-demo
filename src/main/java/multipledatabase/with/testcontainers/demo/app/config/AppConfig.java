package multipledatabase.with.testcontainers.demo.app.config;

import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.IOException;
import java.util.stream.Stream;

@Configuration
@EnableAspectJAutoProxy
@Import({//
        FooDBConfig.class,//
        BarDBConfig.class,//
        BuzDBConfig.class,//
})
@ComponentScan(basePackages = {
        "multipledatabase.with.testcontainers.demo.app.component",
})
public class AppConfig {

    @Bean("propertyPlaceholderConfigurer")
    public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {

        final PropertySourcesPlaceholderConfigurer propertyConfigurer = new PropertySourcesPlaceholderConfigurer();
        final PathMatchingResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();

        propertyConfigurer.setIgnoreUnresolvablePlaceholders(true);
        propertyConfigurer.setIgnoreResourceNotFound(true);
        propertyConfigurer.setLocations(Stream.of(//
                "classpath:application.properties"//
        ).flatMap(locationPattern -> {
            try {
                return Stream.of(resourcePatternResolver.getResources(locationPattern));
            } catch (IOException e) {
                return Stream.empty();
            }
        }).toArray(Resource[]::new));

        return propertyConfigurer;
    }
}
