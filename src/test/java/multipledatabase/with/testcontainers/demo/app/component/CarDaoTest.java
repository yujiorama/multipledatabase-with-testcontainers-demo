package multipledatabase.with.testcontainers.demo.app.component;

import multipledatabase.with.testcontainers.demo.test.config.TestConfig;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.GenericContainer;

import java.math.BigInteger;

@ContextConfiguration(classes = {TestConfig.class})
public class CarDaoTest {

    @SuppressWarnings("rawtypes")
    @ClassRule(order = 0)
    public static GenericContainer MYSQL_CONTAINER = TestConfig.MYSQL_CONTAINER;

    @ClassRule(order = 1)
    public static final SpringClassRule SPRING_CLASS_RULE = new SpringClassRule();

    @Rule
    public final SpringMethodRule springMethodRule = new SpringMethodRule();

    @Autowired
    private CarDao carDao;

    @Test
    @Transactional("buzTransactionManager")
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,//
            scripts = {"classpath:multipledatabase/with/testcontainers/demo/app/component/CarDaoTest/testFindById.sql"},//
            config = @SqlConfig(dataSource = "buzDataSource", transactionManager = "buzTransactionManager")//
    )
    public void testFindById() {

        final BigInteger id = new BigInteger("1374");
        final var actual = this.carDao.findById(id);
        Assert.assertEquals(id, actual.getId());
    }
}