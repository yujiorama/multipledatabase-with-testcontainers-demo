package multipledatabase.with.testcontainers.demo.test.config;

import org.slf4j.LoggerFactory;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.containers.output.Slf4jLogConsumer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.images.builder.ImageFromDockerfile;

import java.time.Duration;
import java.util.Map;
import java.util.Optional;

public class ContainerConfig {
    /**
     * テスト用 MySQL コンテナのユーザー名
     */
    public static final String DB_USER_NAME = "test";

    /**
     * テスト用 MySQL コンテナのパスワード
     */
    public static final String DB_USER_PASSWORD = "test";

    /**
     * テスト用 MySQL コンテナが公開するポート番号（外部からアクセスできるポート番号ではない）
     */
    public static final Integer DB_PORT = MySQLContainer.MYSQL_PORT;

    /**
     * テスト用 MySQL コンテナのイメージ名とタグ
     */
    public static final String DB_IMAGE_NAME = MySQLContainer.IMAGE + ":" + MySQLContainer.DEFAULT_TAG;

    private static final String DEFAULT_DB_IMAGE_NAME = "testdb:test";

    private static final String[] DEFAULT_DB_COMMAND = {"mysqld", "--character-set-server=utf8mb4", "--collation-server=utf8mb4_general_ci"};

    private static final Duration DEFAULT_DB_STARTUP_TIMEOUT_SECOND = Duration.ofSeconds(60);

    @SuppressWarnings({"rawtypes"})
    private static GenericContainer INSTANCE;

    /**
     * {@link org.junit.ClassRule} のフィールドで MySQL のコンテナを開始する。 <br />
     * MySQL は <code>src/test/resources/db</code> に配置した <code>*.sql</code> や <code>*.sh</code> で初期化する。 <br />
     * 作成したコンテナはテストケースの終了と共に破棄される。 <br />
     *
     * @return {@link GenericContainer}
     */
    @SuppressWarnings({"rawtypes"})
    public static GenericContainer mysqlContainer() {

        final ImageFromDockerfile imageFromDockerfile = new ImageFromDockerfile(ContainerConfig.DEFAULT_DB_IMAGE_NAME, true);

        return ContainerConfig.mysqlContainer(imageFromDockerfile, ContainerConfig.DEFAULT_DB_COMMAND, Optional.of(ContainerConfig.DB_PORT));
    }

    /**
     * {@link org.junit.ClassRule} のフィールドで MySQL のコンテナを開始する。 <br />
     * MySQL は <code>src/test/resources/db</code> に配置した <code>*.sql</code> や <code>*.sh</code> で初期化する。 <br />
     * 作成したコンテナはテストケースの終了と共に破棄される。 <br />
     *
     * @param imageName    コンテナのイメージ名を指定する。
     * @param deleteOnExit コンテナを破棄せずに残すかどうかを指定する。
     * @return {@link GenericContainer}
     */
    @SuppressWarnings({"rawtypes"})
    public static GenericContainer mysqlContainer(final String imageName, final boolean deleteOnExit) {

        final ImageFromDockerfile imageFromDockerfile = new ImageFromDockerfile(imageName, deleteOnExit);

        return ContainerConfig.mysqlContainer(imageFromDockerfile, ContainerConfig.DEFAULT_DB_COMMAND, Optional.of(ContainerConfig.DB_PORT));
    }

    /**
     * {@link org.junit.ClassRule} のフィールドで MySQL のコンテナを開始する。 <br />
     * MySQL は <code>src/test/resources/db</code> に配置した <code>*.sql</code> や <code>*.sh</code> で初期化する。 <br />
     * 作成したコンテナはテストケースの終了と共に破棄される。 <br />
     *
     * @param imageName    コンテナのイメージ名を指定する。
     * @param deleteOnExit コンテナを破棄せずに残すかどうかを指定する。
     * @param command      実行するコマンド文字列配列を指定する。
     * @param exposedPort  公開ポート番号を指定する。
     * @return {@link GenericContainer}
     */
    @SuppressWarnings({"rawtypes", "OptionalUsedAsFieldOrParameterType"})
    public static GenericContainer mysqlContainer(final String imageName, final boolean deleteOnExit, final String[] command, final Optional<Integer> exposedPort) {

        final ImageFromDockerfile imageFromDockerfile = new ImageFromDockerfile(imageName, deleteOnExit);

        return ContainerConfig.mysqlContainer(imageFromDockerfile, command, exposedPort);
    }

    /**
     * {@link org.junit.ClassRule} のフィールドで MySQL のコンテナを開始する。 <br />
     * MySQL は <code>src/test/resources/db</code> に配置した <code>*.sql</code> や <code>*.sh</code> で初期化する。 <br />
     * 作成したコンテナはテストケースの終了と共に破棄される。 <br />
     *
     * @param imageFromDockerfile testcontainers の提供するビルダーオブジェクト
     * @param command             実行するコマンド文字列配列を指定する。
     * @param exposedPort         公開ポート番号を指定する。
     * @return {@link GenericContainer}
     */
    @SuppressWarnings({"rawtypes", "OptionalUsedAsFieldOrParameterType"})
    private static GenericContainer mysqlContainer(final ImageFromDockerfile imageFromDockerfile, final String[] command, final Optional<Integer> exposedPort) {

        if (INSTANCE != null) {
            return INSTANCE;
        }

        final GenericContainer container0 = new GenericContainer(//
                imageFromDockerfile//
                        .withDockerfileFromBuilder(builder -> builder//
                                .from(ContainerConfig.DB_IMAGE_NAME)//
                                .env("DEBIAN_FRONTEND", "noninteractive")//
                                .run("apt-get update")//
                                .run("apt-get install -y locales locales-all")//
                                .run("locale-gen ja_JP.UTF-8")//
                                .copy("*.sql", "/docker-entrypoint-initdb.d/")//
                                .copy("*.sh", "/docker-entrypoint-initdb.d/")//
                                .copy("*.txt", "/docker-entrypoint-initdb.d/")//
                                .expose(ContainerConfig.DB_PORT)//
                                .build())//
                        .withFileFromClasspath(".", "/db")//
        ).withEnv(Map.of(//
                "TZ", "Asia/Tokyo", //
                "LANG", "ja_JP.UTF-8", //
                "MYSQL_DATABASE", "test", //
                "MYSQL_USERNAME", ContainerConfig.DB_USER_NAME, //
                "MYSQL_PASSWORD", ContainerConfig.DB_USER_PASSWORD, //
                "MYSQL_ROOT_PASSWORD", ContainerConfig.DB_USER_PASSWORD//
        ))//
                .withCommand(command)//
                .withLogConsumer(new Slf4jLogConsumer(LoggerFactory.getLogger("mysqlContainer")));

        INSTANCE = exposedPort.map(portNumber -> container0//
                .withExposedPorts(portNumber)
                .waitingFor(//
                        Wait.forListeningPort()//
                                .withStartupTimeout(ContainerConfig.DEFAULT_DB_STARTUP_TIMEOUT_SECOND)))
                .orElse(container0);
        return INSTANCE;
    }
}
