package multipledatabase.with.testcontainers.demo.test.config;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.testcontainers.containers.GenericContainer;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Map;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
public class FooDBConfig {

    @Bean("fooDataSource")
    public DataSource dataSource() {

        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        @SuppressWarnings("rawtypes") final GenericContainer mysqlContainer = TestConfig.MYSQL_CONTAINER;
        final String url = String.format("jdbc:mysql://%s:%s/foo?useSSL=false", //
                mysqlContainer.getContainerIpAddress(), //
                mysqlContainer.getMappedPort(ContainerConfig.DB_PORT));
        dataSource.setUrl(url);
        dataSource.setUsername(ContainerConfig.DB_USER_NAME);
        dataSource.setPassword(ContainerConfig.DB_USER_PASSWORD);

        return dataSource;
    }

    @Bean("fooEntityManagerFactory")
    public EntityManagerFactory entityManagerFactory(@Qualifier("fooDataSource") final DataSource dataSource) {

        final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setPackagesToScan("multipledatabase.with.testcontainers.demo.app.entity");
        factoryBean.setPersistenceUnitName("foo");
        factoryBean.setPersistenceProvider(new HibernatePersistenceProvider());
        factoryBean.setJpaVendorAdapter(this.jpaVendorAdapter());
        factoryBean.setJpaProperties(this.jpaProperties());
        factoryBean.afterPropertiesSet();

        return factoryBean.getObject();
    }


    @Bean("fooTransactionManager")
    public PlatformTransactionManager transactionManager(@Qualifier("fooEntityManagerFactory") final EntityManagerFactory entityManagerFactory) {

        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        transactionManager.afterPropertiesSet();

        return transactionManager;

    }

    @Bean("fooJpaVendorAdapter")
    public JpaVendorAdapter jpaVendorAdapter() {

        final HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
        jpaVendorAdapter.setDatabasePlatform("org.hibernate.dialect.MySQLDialect");
        jpaVendorAdapter.setGenerateDdl(false);
        jpaVendorAdapter.setShowSql(false);

        return jpaVendorAdapter;
    }

    @Bean("fooJpaProperties")
    public Properties jpaProperties() {

        final Properties properties = new Properties();
        properties.putAll(Map.of(//
                "hibernate.hbm2ddl.auto", "none", //
                "hibernate.jdbc.batch_size", "30"//
        ));

        return properties;
    }

}
