package multipledatabase.with.testcontainers.demo.test.config;

import org.springframework.context.annotation.*;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.testcontainers.containers.GenericContainer;

@Configuration
@EnableAspectJAutoProxy
@Import({//
        FooDBConfig.class,//
        BarDBConfig.class,//
        BuzDBConfig.class,//
})
@ComponentScan(basePackages = {"multipledatabase.with.testcontainers.demo.app"})
public class TestConfig {
    @SuppressWarnings("rawtypes")
    public static GenericContainer MYSQL_CONTAINER = ContainerConfig.mysqlContainer();

    @Bean
    public PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslationPostProcessor() {

        return new PersistenceExceptionTranslationPostProcessor();
    }
}
