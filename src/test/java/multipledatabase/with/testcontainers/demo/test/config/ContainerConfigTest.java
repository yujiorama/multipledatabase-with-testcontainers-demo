// Copyright(c) 2019 GROWTH XPARTNERS, Incorporated.
//
//
package multipledatabase.with.testcontainers.demo.test.config;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.startupcheck.OneShotStartupCheckStrategy;

import java.io.IOException;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * {@link ContainerConfig} のテストケース。
 */
public class ContainerConfigTest {

    @Test
    public void containerContainsClasspathResources() throws IOException {
        @SuppressWarnings("rawtypes") final GenericContainer container = ContainerConfig.mysqlContainer("testdb:test", true, //
                new String[]{"ls", "-l", "/docker-entrypoint-initdb.d"}, Optional.empty())//
                .withStartupCheckStrategy(new OneShotStartupCheckStrategy())//
                ;
        container.start();
        final String result = container.getLogs();

        final PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();

        Stream.of(resolver.getResources("classpath:db/*"))//
                .forEach(resource -> //
                        Assert.assertTrue(//
                                String.format("container contains file %s", resource.getFilename()), //
                                result.contains(resource.getFilename())));
    }
}
