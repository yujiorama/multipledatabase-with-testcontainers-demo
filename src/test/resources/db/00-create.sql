CREATE DATABASE IF NOT EXISTS foo DEFAULT CHARACTER SET utf8mb4;
CREATE DATABASE IF NOT EXISTS bar DEFAULT CHARACTER SET utf8mb4;
CREATE DATABASE IF NOT EXISTS buz DEFAULT CHARACTER SET utf8mb4;

GRANT ALL PRIVILEGES ON foo.* TO test@'%' IDENTIFIED BY 'test';
GRANT ALL PRIVILEGES ON bar.* TO test@'%' IDENTIFIED BY 'test';
GRANT ALL PRIVILEGES ON buz.* TO test@'%' IDENTIFIED BY 'test';

FLUSH PRIVILEGES;
